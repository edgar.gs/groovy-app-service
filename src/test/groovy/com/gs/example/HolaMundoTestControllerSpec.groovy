package com.gs.example

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class HolaMundoTestControllerSpec extends Specification{

    @Inject
    @Client('/')
    RxHttpClient client

    def "test index"(){
        given:
        println "Test method blocking"
        HttpResponse response = client.toBlocking().exchange("/holaMundo")

        expect:
        response.status == HttpStatus.OK
    }
}
