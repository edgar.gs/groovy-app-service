package com.gs.example

import io.micronaut.context.ApplicationContext
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.reactivex.Maybe
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class HolaMundoControllerSpec extends Specification {

    @Shared
    @AutoCleanup
    EmbeddedServer embeddedServer = ApplicationContext.run(EmbeddedServer)

    @Shared
    @AutoCleanup
    RxHttpClient client = embeddedServer.applicationContext.createBean(RxHttpClient, embeddedServer.getURL())

    void "test index"() {
        given:
        println "Test method blocking"
        HttpResponse response = client.toBlocking().exchange("/holaMundo")

        expect:
        response.status == HttpStatus.OK
    }

    void "test index 2"() {
        given:
        println 'Test method reactive'
        Maybe<HttpResponse> response = client.exchange("/holaMundo").firstElement()
        response.subscribe(
                {v -> println("1000th visitor: " + v + " won the prize")},
                {ex -> println("Error: " + ex.getMessage())},
                { -> println("We need more marketing")
                }
        )
        expect:
        response.test().assertNoErrors()
    }
}
