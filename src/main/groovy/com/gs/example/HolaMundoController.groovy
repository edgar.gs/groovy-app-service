package com.gs.example

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.HttpStatus


@Controller("/holaMundo")
class HolaMundoController {

    @Get("/")
    HttpStatus index() {
        return HttpStatus.OK
    }
}