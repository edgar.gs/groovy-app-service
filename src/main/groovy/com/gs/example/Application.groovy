package com.gs.example

import groovy.util.logging.Slf4j
import io.micronaut.runtime.Micronaut
import groovy.transform.CompileStatic

@CompileStatic
@Slf4j
class Application {
    static void main(String[] args) {
        log.debug "Inicio Servicio"
        Micronaut.run(Application)
    }
}