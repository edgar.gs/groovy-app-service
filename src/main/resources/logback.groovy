appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n"
    }
}
root(INFO, ["STDOUT"])

appender("FILE", RollingFileAppender) {
    file = "logFile.log"
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "logFile.%d{yyyy-MM-dd}.zip"
        maxHistory = 30
        totalSizeCap = "3GB"
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%-4relative [%thread] %-5level %logger{35} - %msg%n"
    }
}
root(INFO, ["FILE"])

logger("com.gs.example", DEBUG)